require("dotenv").config()
const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const users = require("./models/users.js");
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const posts = [
    {
        username:"Kushal",
        title:"Post-1"
    },
    {
        username:"Ruchi",
        title:"Post-2"
    }
];

app.get("/posts",(req,res)=>{     
    res.json({posts});
});
app.post("/login",(req,res)=>{
    let name = users.username;
    console.log("Username is:-",req.body.username);        
    const payload = {name:req.body.username}        
    const token = jwt.sign(payload,process.env.ACCESS_TOKEN_SECRET);
    if(token == null){
        res.json({token:token});
    }
});

app.listen(3000);

